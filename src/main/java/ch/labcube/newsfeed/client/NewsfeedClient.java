package ch.labcube.newsfeed.client;




import ch.labcube.newsfeed.api.swagger.handler.ApiClient;
import ch.labcube.newsfeed.api.swagger.handler.api.RestNewsServiceApi;
import ch.labcube.newsfeed.api.swagger.model.News;
import ch.labcube.newsfeed.api.swagger.model.NewsConsumation;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class NewsfeedClient {
    private final RestNewsServiceApi api;

    public NewsfeedClient() {
        api = new RestNewsServiceApi();
    }

    /**
     * Use this Constructor to Inject a RestTemplate for the Calls.
     * IE. you want to use this to pass on the bearer Token etc.
     * @param restTemplate
     */
    public NewsfeedClient(Object restTemplate) {
        api = new RestNewsServiceApi(new ApiClient((RestTemplate) restTemplate));
    }

    public List<News> getNewsForProductAndUsername(String product, String username, String language) {
        return getNewsForProductAndUsername(product,username,language,null);
    }

    public List<News> getNewsForProductAndUsername(String product, String username, String language, String bearerToken) {
        return api.getNewsForProductAndUserUsingGET(product, username, language, bearerToken);
    }

    public List<News> getAllNews(Integer limit, String language) {
        return getAllNews(limit,language,null);
    }

    public List<News> getAllNews(Integer limit, String language, String bearerToken) {
        return api.getAllNewsUsingGET(limit, language, bearerToken);
    }

    public void storeAsRead(News news, String username) {
        api.saveNewsReadUsingPOST(new NewsConsumation().news(news).username(username));
    }
}
