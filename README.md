Import this in your Build/Plugin Section from Maven:

            <!--*************************************************-->
            <!-- GET NEWSFEED JAR          -->
            <!--*************************************************-->


            <plugin>
                <groupId>com.googlecode.maven-download-plugin</groupId>
                <artifactId>download-maven-plugin</artifactId>
                <version>1.4.2</version>
                <executions>
                    <execution>
                        <id>install-newsfeed</id>
                        <phase>validate</phase>
                        <goals>
                            <goal>wget</goal>
                        </goals>
                        <configuration>
                            <url>https://bitbucket.org/ufdevelopers/newsfeed-client/downloads/newsfeed-client-RELEASE.jar</url>
                            <overwrite>true</overwrite>
                            <skipCache>true</skipCache>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-install-plugin</artifactId>
                <version>2.5.1</version>
                <configuration>
                    <file>${basedir}/target/newsfeed-client-RELEASE.jar</file>
                </configuration>
                <executions>
                    <execution>
                        <id>install-newsfeed-lib</id>
                        <goals>
                            <goal>install-file</goal>
                        </goals>
                        <phase>validate</phase>
                    </execution>
                </executions>
            </plugin>
        </plugins>

And then also in dependencies:

        <!-- NEWSFEED -->
        <dependency>
            <groupId>ch.labcube.newsfeed</groupId>
            <artifactId>newsfeed-client</artifactId>
            <version>RELEASE</version>
        </dependency>
        
        
        